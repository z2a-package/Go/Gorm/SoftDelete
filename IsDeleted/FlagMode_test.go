package IsDeleted

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"

	"gitlab.com/z2a-package/Go/Gorm/SoftDelete/internal/Test"
)

type FlagUser struct {
	ID        uint
	Name      string
	Age       uint
	IsDeleted IsDeleted
}

type FlagModeItem struct {
	database *gorm.DB
	data     *FlagUser
}

type FlagModeSuite struct {
	suite.Suite

	noDeleted   *FlagModeItem
	softDeleted *FlagModeItem
	hardDeleted *FlagModeItem

	table FlagUser
}

func (it *FlagModeSuite) setupDatabase(name string, data FlagUser) (result *FlagModeItem) {
	test := it.T()

	result = &FlagModeItem{}
	result.database = Test.SetupOpenDatabase(test, name, it.table)
	result.data = &data

	throw := result.database.Save(result.data).Error
	it.Require().NoErrorf(throw, "failed to save data: %s", throw)
	return
}

func (it *FlagModeSuite) loadItem(item *FlagModeItem) (database *gorm.DB, data *FlagUser) {
	database = item.database
	data = item.data
	return
}

func (it *FlagModeSuite) SetupSuite() {
	var throw error
	it.table = FlagUser{}
	data := FlagUser{Name: "jinzhu", Age: 20}

	it.noDeleted = it.setupDatabase("Flag.noDeleted.sqlite", data)
	it.softDeleted = it.setupDatabase("Flag.softDeleted.sqlite", data)
	it.hardDeleted = it.setupDatabase("Flag.hardDeleted.sqlite", data)

	throw = it.softDeleted.database.Delete(it.softDeleted.data).Error
	it.Require().NoErrorf(throw, "No error should happen when soft delete data, but got %v", throw)

	throw = it.hardDeleted.database.Unscoped().Delete(it.hardDeleted.data).Error
	it.Require().NoErrorf(throw, "No error should happen when hard delete data, but got %v", throw)
}

func (it *FlagModeSuite) TestNoDeletedCount() {
	database, data := it.loadItem(it.noDeleted)

	var actual, expect int64
	expect = 1

	throw := database.Model(it.table).Where("name = ?", data.Name).Count(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Count soft deleted record, expect: %v, actual: %v", expect, actual)
	}
}

func (it *FlagModeSuite) TestNoDeletedAge() {
	database, data := it.loadItem(it.noDeleted)

	var actual, expect uint
	expect = data.Age

	throw := database.Model(it.table).Select("age").Where("name = ?", data.Name).Scan(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Age soft deleted record, expect: %v, actual: %v", expect, actual)
	}
}

func (it *FlagModeSuite) TestSoftDeletedIsDeleted() {
	_, data := it.loadItem(it.softDeleted)
	var actual, expect bool
	expect = true
	actual = bool(data.IsDeleted)

	it.Equalf(expect, actual, "data's deleted flag should be true, IsDeleted: %v", actual)
}

func (it *FlagModeSuite) TestSoftDeletedSqlDelete() {
	database, data := it.loadItem(it.softDeleted)
	pattern := `UPDATE .flag_users. SET .is_deleted.=.* WHERE .flag_users.\..id. = .* AND .flag_users.\..is_deleted. = ?`
	sql := database.Session(&gorm.Session{DryRun: true}).Delete(data).Statement.SQL.String()
	it.Regexpf(pattern, sql, "invalid sql generated, got %v", sql)
}

func (it *FlagModeSuite) TestSoftDeletedFirst() {
	database, data := it.loadItem(it.softDeleted)
	table := it.table

	throw := database.First(&table, "name = ?", data.Name).Error
	it.ErrorIsf(throw, gorm.ErrRecordNotFound, "Can'test find a soft deleted record: %s", throw)
}

func (it *FlagModeSuite) TestSoftDeletedCount() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect int64
	expect = 0

	throw := database.Model(it.table).Where("name = ?", data.Name).Count(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Count soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *FlagModeSuite) TestSoftDeletedAge() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect uint
	expect = 0

	throw := database.Model(it.table).Select("age").Where("name = ?", data.Name).Scan(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Age soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *FlagModeSuite) TestSoftDeletedUnscopedFirst() {
	database, data := it.loadItem(it.softDeleted)
	table := it.table

	throw := database.Unscoped().First(&table, "name = ?", data.Name).Error
	it.Nilf(throw, "Should find soft deleted record with Unscoped, but got throw %s", throw)
}

func (it *FlagModeSuite) TestSoftDeletedUnscopedCount() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect int64
	expect = 1

	throw := database.Unscoped().Model(it.table).Where("name = ?", data.Name).Count(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Count soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *FlagModeSuite) TestSoftDeletedUnscopedAge() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect uint
	expect = data.Age

	throw := database.Unscoped().Model(it.table).Select("age").Where("name = ?", data.Name).Scan(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Age soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *FlagModeSuite) TestHardDeletedUnscopedFirst() {
	database, data := it.loadItem(it.hardDeleted)
	table := it.table

	throw := database.Unscoped().First(&table, "name = ?", data.Name).Error
	it.ErrorIsf(throw, gorm.ErrRecordNotFound, "Can'test find hard deleted record: %s", throw)
}

func TestFlagModeSuite(test *testing.T) {
	suite.Run(test, new(FlagModeSuite))
}
