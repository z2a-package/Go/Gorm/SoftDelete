package IsDeleted

import (
	"github.com/spf13/cast"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/schema"
)

const (
	FlagDeleted = true
	FlagActived = false
)

type IsDeleted bool

func (it *IsDeleted) QueryClauses(field *schema.Field) []clause.Interface {
	return []clause.Interface{&QueryClause{Field: field}}
}

func (it *IsDeleted) UpdateClauses(field *schema.Field) []clause.Interface {
	return []clause.Interface{&UpdateClause{Field: field}}
}

func (it *IsDeleted) DeleteClauses(field *schema.Field) []clause.Interface {
	return []clause.Interface{&DeleteClause{Field: field}}
}

func (it *IsDeleted) Scan(value any) (throw error) {
	var boolean bool

	switch value.(type) {
	case string:
		boolean = "" != value
	default:
		boolean = cast.ToBool(value)
	}

	*it = IsDeleted(boolean)
	return
}

func GetDefaultValue(field *schema.Field) (result any) {
	if "null" == field.DefaultValue {
		return nil
	} else {
		return FlagActived
	}
}
