package IsDeleted

import (
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/schema"

	"gitlab.com/z2a-package/Go/Gorm/SoftDelete/internal/Helper/Delete"
)

type DeleteClause struct{ Field *schema.Field }

func (it *DeleteClause) Name() string { return "" }

func (it *DeleteClause) Build(data clause.Builder) {}

func (it *DeleteClause) MergeClause(data *clause.Clause) {}

func (it *DeleteClause) ModifyStatement(data *gorm.Statement) {
	field := it.Field
	query := &QueryClause{Field: field}
	Delete.ModifyStatement(data, field.DBName, FlagDeleted, query)
}
