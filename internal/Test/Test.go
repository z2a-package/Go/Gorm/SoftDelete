package Test

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/glebarez/sqlite"
	"github.com/stretchr/testify/require"
	"gorm.io/gorm"
)

func SetupOpenDatabase(test *testing.T, name string, tables ...any) (result *gorm.DB) {
	result, throw := gorm.Open(sqlite.Open(filepath.Join(os.TempDir(), name)), &gorm.Config{})

	require.NoErrorf(test, throw, "failed to connect result: %s", throw)

	result = result.Debug()

	throw = result.Migrator().DropTable(tables...)
	require.NoErrorf(test, throw, "failed to drop table: %s", throw)

	throw = result.AutoMigrate(tables...)
	require.NoErrorf(test, throw, "failed to auto migrate: %s", throw)

	return
}
