package Update

import (
	"gorm.io/gorm"

	"gitlab.com/z2a-package/Go/Gorm/SoftDelete/internal/Helper"
)

func ModifyStatement(data *gorm.Statement, query gorm.StatementModifier) {
	if Helper.SkipModify(data) {
		return
	}

	query.ModifyStatement(data)
}
