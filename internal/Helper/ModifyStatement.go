package Helper

import (
	"gorm.io/gorm"
)

func SkipModify(data *gorm.Statement) bool {
	return 0 != data.SQL.Len() || data.Statement.Unscoped
}
