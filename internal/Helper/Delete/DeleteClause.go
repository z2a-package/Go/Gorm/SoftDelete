package Delete

import (
	"reflect"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/schema"

	"gitlab.com/z2a-package/Go/Gorm/SoftDelete/internal/Helper"
)

func HandlePrimaryFieldClause(data *gorm.Statement) {
	if nil == data.Schema {
		return
	}

	AddPrimaryFieldClause(data, data.ReflectValue)

	if false == data.ReflectValue.CanAddr() || data.Dest == data.Model || nil == data.Model {
		return
	}

	AddPrimaryFieldClause(data, reflect.ValueOf(data.Model))
}

func AddPrimaryFieldClause(data *gorm.Statement, reflectValue reflect.Value) {
	_, queryValues := schema.GetIdentityFieldValuesMap(data.Context, reflectValue, data.Schema.PrimaryFields)
	column, values := schema.ToQueryValues(data.Table, data.Schema.PrimaryFieldDBNames, queryValues)

	if 0 < len(values) {
		data.AddClause(clause.Where{Exprs: []clause.Expression{clause.IN{Column: column, Values: values}}})
	}
}

func ModifyStatement(data *gorm.Statement, name string, value any, query gorm.StatementModifier) {
	if Helper.SkipModify(data) {
		return
	}

	var set clause.Set
	set = append(clause.Set{{Column: clause.Column{Name: name}, Value: value}}, set...)
	data.AddClause(set)
	data.SetColumn(name, value, true)

	HandlePrimaryFieldClause(data)

	query.ModifyStatement(data)
	data.AddClauseIfNotExists(clause.Update{})
	data.Build(data.DB.Callback().Update().Clauses...)
}
