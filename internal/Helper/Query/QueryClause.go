package Query

import (
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const ClauseKey = "soft_delete_enabled"

func WrapOrCondition(data *gorm.Statement) {
	whereClause, ok := data.Clauses["WHERE"]

	if false == ok {
		return
	}

	where, ok := whereClause.Expression.(clause.Where)

	if false == ok || 0 == len(where.Exprs) {
		return
	}

	for _, expr := range where.Exprs {
		orCondition, ok := expr.(clause.OrConditions)

		if false == ok || 1 != len(orCondition.Exprs) {
			continue
		}

		where.Exprs = []clause.Expression{clause.And(where.Exprs...)}
		whereClause.Expression = where
		data.Clauses["WHERE"] = whereClause
		break
	}
}

func ModifyStatement(data *gorm.Statement, name string, value any) {
	_, ok := data.Clauses[ClauseKey]

	if ok || data.Statement.Unscoped {
		return
	}

	WrapOrCondition(data)

	column := clause.Column{Table: clause.CurrentTable, Name: name}
	equal := clause.Eq{Column: column, Value: value}
	where := clause.Where{Exprs: []clause.Expression{equal}}

	data.AddClause(where)
	data.Clauses[ClauseKey] = clause.Clause{}
}
