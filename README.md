# Soft Delete

Inspired `gorm.io/plugin/soft_delete`, No Mixed Mode

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/z2a-package/Go/Gorm/SoftDelete.svg)](https://pkg.go.dev/gitlab.com/z2a-package/Go/Gorm/SoftDelete)

## DeletedAt

- use `int64` save unix timestamp
- support time type: `nano`, `micro`, `milli`, `second`
- example:

```go
import "gitlab.com/z2a-package/Go/Gorm/SoftDelete/DeletedAt"

type User struct {
  ID        uint
  Name      string
  DeletedAt DeletedAt.DeletedAt
//  DeletedAt DeletedAt.DeletedAt `gorm:"SoftDelete:Nano"`
//  DeletedAt DeletedAt.DeletedAt `gorm:"SoftDelete:Micro"`
//  DeletedAt DeletedAt.DeletedAt `gorm:"SoftDelete:Milli"`
}

// Query
SELECT * FROM users WHERE deleted_at = 0;

// Delete
UPDATE users SET deleted_at = /* current unix second */ WHERE ID = 1;
```

## IsDeleted

- use `bool` save deleted flag
- example:

```go
import "gitlab.com/z2a-package/Go/Gorm/SoftDelete/IsDeleted"

type User struct {
  ID        uint
  Name      string
  IsDeleted IsDeleted.IsDeleted
}

// Query
SELECT * FROM users WHERE is_deleted = 0;

// Delete
UPDATE users SET is_deleted = 1 WHERE ID = 1;
```
