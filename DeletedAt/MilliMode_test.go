package DeletedAt

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"

	"gitlab.com/z2a-package/Go/Gorm/SoftDelete/internal/Test"
)

type MilliUser struct {
	ID        uint
	Name      string
	Age       uint
	DeletedAt DeletedAt `gorm:"SoftDelete:Milli"`
}

type MilliModeItem struct {
	database *gorm.DB
	data     *MilliUser
}

type MilliModeSuite struct {
	suite.Suite

	noDeleted   *MilliModeItem
	softDeleted *MilliModeItem
	hardDeleted *MilliModeItem

	table MilliUser
}

func (it *MilliModeSuite) setupDatabase(name string, data MilliUser) (result *MilliModeItem) {
	test := it.T()

	result = &MilliModeItem{}
	result.database = Test.SetupOpenDatabase(test, name, it.table)
	result.data = &data

	throw := result.database.Save(result.data).Error
	it.Require().NoErrorf(throw, "failed to save data: %s", throw)
	return
}

func (it *MilliModeSuite) loadItem(item *MilliModeItem) (database *gorm.DB, data *MilliUser) {
	database = item.database
	data = item.data
	return
}

func (it *MilliModeSuite) SetupSuite() {
	var throw error
	it.table = MilliUser{}
	data := MilliUser{Name: "jinzhu", Age: 20}

	it.noDeleted = it.setupDatabase("Milli.noDeleted.sqlite", data)
	it.softDeleted = it.setupDatabase("Milli.softDeleted.sqlite", data)
	it.hardDeleted = it.setupDatabase("Milli.hardDeleted.sqlite", data)

	throw = it.softDeleted.database.Delete(it.softDeleted.data).Error
	it.Require().NoErrorf(throw, "No error should happen when soft delete data, but got %v", throw)

	throw = it.hardDeleted.database.Unscoped().Delete(it.hardDeleted.data).Error
	it.Require().NoErrorf(throw, "No error should happen when hard delete data, but got %v", throw)
}

func (it *MilliModeSuite) TestNoDeletedCount() {
	database, data := it.loadItem(it.noDeleted)

	var actual, expect int64
	expect = 1

	throw := database.Model(it.table).Where("name = ?", data.Name).Count(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Count soft deleted record, expect: %v, actual: %v", expect, actual)
	}
}

func (it *MilliModeSuite) TestNoDeletedAge() {
	database, data := it.loadItem(it.noDeleted)

	var actual, expect uint
	expect = data.Age

	throw := database.Model(it.table).Select("age").Where("name = ?", data.Name).Scan(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Age soft deleted record, expect: %v, actual: %v", expect, actual)
	}
}

func (it *MilliModeSuite) TestSoftDeletedIsDeleted() {
	_, data := it.loadItem(it.softDeleted)
	var actual, expect int64
	expect = 0
	actual = int64(data.DeletedAt)
	it.NotEqualf(expect, actual, "data's deleted at should not be zero, DeletedAt: %v", actual)
}

func (it *MilliModeSuite) TestSoftDeletedSqlDelete() {
	database, data := it.loadItem(it.softDeleted)
	pattern := `UPDATE .milli_users. SET .deleted_at.=.* WHERE .milli_users.\..id. = .* AND .milli_users.\..deleted_at. = ?`
	sql := database.Session(&gorm.Session{DryRun: true}).Delete(data).Statement.SQL.String()
	it.Regexpf(pattern, sql, "invalid sql generated, got %v", sql)
}

func (it *MilliModeSuite) TestSoftDeletedFirst() {
	database, data := it.loadItem(it.softDeleted)
	table := it.table

	throw := database.First(&table, "name = ?", data.Name).Error
	it.ErrorIsf(throw, gorm.ErrRecordNotFound, "Can'test find a soft deleted record: %s", throw)
}

func (it *MilliModeSuite) TestSoftDeletedCount() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect int64
	expect = 0

	throw := database.Model(it.table).Where("name = ?", data.Name).Count(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Count soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *MilliModeSuite) TestSoftDeletedAge() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect uint
	expect = 0

	throw := database.Model(it.table).Select("age").Where("name = ?", data.Name).Scan(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Age soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *MilliModeSuite) TestSoftDeletedUnscopedFirst() {
	database, data := it.loadItem(it.softDeleted)
	table := it.table

	throw := database.Unscoped().First(&table, "name = ?", data.Name).Error
	it.Nilf(throw, "Should find soft deleted record with Unscoped, but got throw %s", throw)
}

func (it *MilliModeSuite) TestSoftDeletedUnscopedCount() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect int64
	expect = 1

	throw := database.Unscoped().Model(it.table).Where("name = ?", data.Name).Count(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Count soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *MilliModeSuite) TestSoftDeletedUnscopedAge() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect uint
	expect = data.Age

	throw := database.Unscoped().Model(it.table).Select("age").Where("name = ?", data.Name).Scan(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Age soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *MilliModeSuite) TestHardDeletedUnscopedFirst() {
	database, data := it.loadItem(it.hardDeleted)
	table := it.table

	throw := database.Unscoped().First(&table, "name = ?", data.Name).Error
	it.ErrorIsf(throw, gorm.ErrRecordNotFound, "Can'test find hard deleted record: %s", throw)
}

func TestMilliModeSuite(test *testing.T) {
	suite.Run(test, new(MilliModeSuite))
}
