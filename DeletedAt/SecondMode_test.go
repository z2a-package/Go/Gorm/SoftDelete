package DeletedAt

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"

	"gitlab.com/z2a-package/Go/Gorm/SoftDelete/internal/Test"
)

type SecondUser struct {
	ID        uint
	Name      string
	Age       uint
	DeletedAt DeletedAt
}

type SecondModeItem struct {
	database *gorm.DB
	data     *SecondUser
}

type SecondModeSuite struct {
	suite.Suite

	noDeleted   *SecondModeItem
	softDeleted *SecondModeItem
	hardDeleted *SecondModeItem

	table SecondUser
}

func (it *SecondModeSuite) setupDatabase(name string, data SecondUser) (result *SecondModeItem) {
	test := it.T()

	result = &SecondModeItem{}
	result.database = Test.SetupOpenDatabase(test, name, it.table)
	result.data = &data

	throw := result.database.Save(result.data).Error
	it.Require().NoErrorf(throw, "failed to save data: %s", throw)
	return
}

func (it *SecondModeSuite) loadItem(item *SecondModeItem) (database *gorm.DB, data *SecondUser) {
	database = item.database
	data = item.data
	return
}

func (it *SecondModeSuite) SetupSuite() {
	var throw error
	it.table = SecondUser{}
	data := SecondUser{Name: "jinzhu", Age: 20}

	it.noDeleted = it.setupDatabase("Second.noDeleted.sqlite", data)
	it.softDeleted = it.setupDatabase("Second.softDeleted.sqlite", data)
	it.hardDeleted = it.setupDatabase("Second.hardDeleted.sqlite", data)

	throw = it.softDeleted.database.Delete(it.softDeleted.data).Error
	it.Require().NoErrorf(throw, "No error should happen when soft delete data, but got %v", throw)

	throw = it.hardDeleted.database.Unscoped().Delete(it.hardDeleted.data).Error
	it.Require().NoErrorf(throw, "No error should happen when hard delete data, but got %v", throw)
}

func (it *SecondModeSuite) TestNoDeletedCount() {
	database, data := it.loadItem(it.noDeleted)

	var actual, expect int64
	expect = 1

	throw := database.Model(it.table).Where("name = ?", data.Name).Count(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Count soft deleted record, expect: %v, actual: %v", expect, actual)
	}
}

func (it *SecondModeSuite) TestNoDeletedAge() {
	database, data := it.loadItem(it.noDeleted)

	var actual, expect uint
	expect = data.Age

	throw := database.Model(it.table).Select("age").Where("name = ?", data.Name).Scan(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Age soft deleted record, expect: %v, actual: %v", expect, actual)
	}
}

func (it *SecondModeSuite) TestSoftDeletedIsDeleted() {
	_, data := it.loadItem(it.softDeleted)
	var actual, expect int64
	expect = 0
	actual = int64(data.DeletedAt)
	it.NotEqualf(expect, actual, "data's deleted at should not be zero, DeletedAt: %v", actual)
}

func (it *SecondModeSuite) TestSoftDeletedSqlDelete() {
	database, data := it.loadItem(it.softDeleted)
	pattern := `UPDATE .second_users. SET .deleted_at.=.* WHERE .second_users.\..id. = .* AND .second_users.\..deleted_at. = ?`
	sql := database.Session(&gorm.Session{DryRun: true}).Delete(data).Statement.SQL.String()
	it.Regexpf(pattern, sql, "invalid sql generated, got %v", sql)
}

func (it *SecondModeSuite) TestSoftDeletedFirst() {
	database, data := it.loadItem(it.softDeleted)
	table := it.table

	throw := database.First(&table, "name = ?", data.Name).Error
	it.ErrorIsf(throw, gorm.ErrRecordNotFound, "Can'test find a soft deleted record: %s", throw)
}

func (it *SecondModeSuite) TestSoftDeletedCount() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect int64
	expect = 0

	throw := database.Model(it.table).Where("name = ?", data.Name).Count(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Count soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *SecondModeSuite) TestSoftDeletedAge() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect uint
	expect = 0

	throw := database.Model(it.table).Select("age").Where("name = ?", data.Name).Scan(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Age soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *SecondModeSuite) TestSoftDeletedUnscopedFirst() {
	database, data := it.loadItem(it.softDeleted)
	table := it.table

	throw := database.Unscoped().First(&table, "name = ?", data.Name).Error
	it.Nilf(throw, "Should find soft deleted record with Unscoped, but got throw %s", throw)
}

func (it *SecondModeSuite) TestSoftDeletedUnscopedCount() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect int64
	expect = 1

	throw := database.Unscoped().Model(it.table).Where("name = ?", data.Name).Count(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Count soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *SecondModeSuite) TestSoftDeletedUnscopedAge() {
	database, data := it.loadItem(it.softDeleted)

	var actual, expect uint
	expect = data.Age

	throw := database.Unscoped().Model(it.table).Select("age").Where("name = ?", data.Name).Scan(&actual).Error

	if it.NoErrorf(throw, "failed to query: %s", throw) {
		it.Equalf(expect, actual, "Age soft deleted record, expect: %v, actual: %v", expect, actual)
	}

}

func (it *SecondModeSuite) TestHardDeletedUnscopedFirst() {
	database, data := it.loadItem(it.hardDeleted)
	table := it.table

	throw := database.Unscoped().First(&table, "name = ?", data.Name).Error
	it.ErrorIsf(throw, gorm.ErrRecordNotFound, "Can'test find hard deleted record: %s", throw)
}

func TestSecondModeSuite(test *testing.T) {
	suite.Run(test, new(SecondModeSuite))
}
