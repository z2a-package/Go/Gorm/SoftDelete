package DeletedAt

import (
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/schema"

	"gitlab.com/z2a-package/Go/Gorm/SoftDelete/internal/Helper/Delete"
)

type DeleteClause struct {
	Field    *schema.Field
	TimeType TimeType
}

func (it *DeleteClause) Name() string { return "" }

func (it *DeleteClause) Build(data clause.Builder) {}

func (it *DeleteClause) MergeClause(data *clause.Clause) {}

func (it *DeleteClause) ModifyStatement(data *gorm.Statement) {
	field := it.Field
	value := TimeToUnix(data.DB.NowFunc(), it.TimeType)
	query := &QueryClause{Field: field}
	Delete.ModifyStatement(data, field.DBName, value, query)
}
