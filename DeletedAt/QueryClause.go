package DeletedAt

import (
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/schema"

	"gitlab.com/z2a-package/Go/Gorm/SoftDelete/internal/Helper/Query"
)

type QueryClause struct{ Field *schema.Field }

func (it *QueryClause) Name() string { return "" }

func (it *QueryClause) Build(data clause.Builder) {}

func (it *QueryClause) MergeClause(data *clause.Clause) {}

func (it *QueryClause) ModifyStatement(data *gorm.Statement) {
	field := it.Field
	value := GetDefaultValue(field)
	Query.ModifyStatement(data, field.DBName, value)
}
