package DeletedAt

import (
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/schema"

	"gitlab.com/z2a-package/Go/Gorm/SoftDelete/internal/Helper/Update"
)

type UpdateClause struct{ Field *schema.Field }

func (it *UpdateClause) Name() string { return "" }

func (it *UpdateClause) Build(data clause.Builder) {}

func (it *UpdateClause) MergeClause(data *clause.Clause) {}

func (it *UpdateClause) ModifyStatement(data *gorm.Statement) {
	query := &QueryClause{Field: it.Field}
	Update.ModifyStatement(data, query)
}
