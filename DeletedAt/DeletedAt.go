package DeletedAt

import (
	"time"

	"gorm.io/gorm/clause"
	"gorm.io/gorm/schema"
)

type DeletedAt int64

type TimeType int64

const (
	Unix      TimeType = 0
	UnixMilli TimeType = 3
	UnixMicro TimeType = 6
	UnixNano  TimeType = 9
)

func (it *DeletedAt) QueryClauses(field *schema.Field) []clause.Interface {
	return []clause.Interface{&QueryClause{Field: field}}
}

func (it *DeletedAt) UpdateClauses(field *schema.Field) []clause.Interface {
	return []clause.Interface{&UpdateClause{Field: field}}
}

func (it *DeletedAt) DeleteClauses(field *schema.Field) []clause.Interface {
	settings := schema.ParseTagSetting(field.TagSettings["SOFTDELETE"], ",")
	return []clause.Interface{&DeleteClause{Field: field, TimeType: GetTimeType(settings)}}
}

func GetDefaultValue(field *schema.Field) (result any) {
	if "null" == field.DefaultValue {
		return nil
	} else {
		return 0
	}
}

func GetTimeType(settings map[string]string) TimeType {
	if "" != settings["NANO"] {
		return UnixNano
	}

	if "" != settings["MICRO"] {
		return UnixMicro
	}

	if "" != settings["MILLI"] {
		return UnixMilli
	}

	return Unix
}

func TimeToUnix(timeValue time.Time, timeType TimeType) int64 {
	switch timeType {
	case UnixNano:
		return timeValue.UnixNano()
	case UnixMicro:
		return timeValue.UnixMicro()
	case UnixMilli:
		return timeValue.UnixMilli()
	default:
		return timeValue.Unix()
	}
}
